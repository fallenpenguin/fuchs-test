﻿using UnityEngine;
using System.Collections;

public class performUse : MonoBehaviour {

	public float usabilityRange = 5.0f;

	public GameObject interactPrefab;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetMouseButtonDown (0)) {
			Ray ray = new Ray (Camera.main.transform.position, Camera.main.transform.forward);
			RaycastHit hitInfo;

			if (Physics.Raycast (ray, out hitInfo, usabilityRange)) {
				Vector3 hitPoint = hitInfo.point;
				GameObject go = hitInfo.collider.gameObject;
				//Debug.Log ("Hit Object: " + go.name);
				//Debug.Log ("Hit Point: " + hitPoint);

				hasInteract h = go.GetComponent<hasInteract> ();

				if (h != null) {
					h.toggleText ();
					if (interactPrefab != null) {
						Instantiate (interactPrefab, hitPoint, Quaternion.identity);
					}
				}
			}
		}
	}
}
