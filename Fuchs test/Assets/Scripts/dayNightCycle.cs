﻿using UnityEngine;
using System.Collections;

public class dayNightCycle : MonoBehaviour {

	public float dayDuration = 1.0f;

	float timer;
	float percentOfDay;
	float turnSpeed;

	// Use this for initialization
	void Start () {
		timer = 0.0f;
	}
	
	// Update is called once per frame
	void Update () {
		checkTime();
		UpdateLights();

		turnSpeed = 360.0f / (dayDuration * 60.0f) * Time.deltaTime;
		transform.RotateAround (transform.position, transform.right, turnSpeed);

		//Debug.Log ("% "+percentOfDay);
	}

	void UpdateLights() {
		Light light = GetComponent<Light>();
		if (isNight()) {
			if (light.intensity > 0.0f) {
				light.intensity -= 0.05f;
			}
		}
		else {
			if (light.intensity < 1.0f) {
				light.intensity += 0.05f;
			}
		}

	}

	bool isNight() {
		bool night = false;
		if (percentOfDay > 0.5f) {
			night = true;
		}
		return night;
	}

	void checkTime() {
		timer += Time.deltaTime;
		percentOfDay = timer / (dayDuration * 60.0f);
		if (timer > (dayDuration * 60.0f)) {
			timer = 0.0f;
		}
	}
}
