﻿using UnityEngine;
using System.Collections;

using UnityEngine.UI;

public class hasInteract : MonoBehaviour {

	//public string text = "This text will be shown in the box.";
	bool active = false;
	GameObject go;

	// Use this for initialization
	void Start () {
		go = gameObject;
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void toggleText() {
		if (!active) {
			active = true;;
			foreach (Transform child in go.transform) {
				child.gameObject.SetActive (true);
			}
		}
		else {
			active = false;
			foreach (Transform child in go.transform) {
				child.gameObject.SetActive (false);
			}
		}
	}

}
