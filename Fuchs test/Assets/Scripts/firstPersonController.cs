﻿using UnityEngine;
using System.Collections;

[RequireComponent (typeof(CharacterController))]
public class firstPersonController : MonoBehaviour {

	public float movementSpeed = 10.0f;
	public float mouseSensitivity = 3.0f;
	public float jumpSpeed = 7.5f;

	float verticalRotation = 0;
	public float verticalRotationLimit = 60.0f;

	float verticalVelocity = 0;
	CharacterController characterController;

	// Use this for initialization
	void Start () {
		//Screen.lockCursor = true;
		Cursor.lockState = CursorLockMode.Locked;
		Cursor.visible = false;

		characterController = GetComponent<CharacterController> ();

	}
	
	// Update is called once per frame
	void Update () {
		
		//rotation
		float rotLR = Input.GetAxis("Mouse X") * mouseSensitivity;
		transform.Rotate(0,rotLR,0);

		verticalRotation -= Input.GetAxis ("Mouse Y") * mouseSensitivity;
		verticalRotation = Mathf.Clamp (verticalRotation, -verticalRotationLimit, verticalRotationLimit);
		Camera.main.transform.localRotation = Quaternion.Euler(verticalRotation,0,0);


		//movement
		float forwardSpeed = Input.GetAxis("Vertical") * movementSpeed;
		float sideSpeed = Input.GetAxis("Horizontal") * movementSpeed;

		verticalVelocity += Physics.gravity.y * Time.deltaTime;

		if (characterController.isGrounded && Input.GetButtonDown("Jump")) {
			verticalVelocity = jumpSpeed;
		}

		Vector3 speed = new Vector3 (sideSpeed, verticalVelocity, forwardSpeed);

		speed = transform.rotation * speed;

		characterController.Move(speed * Time.deltaTime);
	}
}
